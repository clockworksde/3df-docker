# 3DF Docker Image

This repository holds a simple base image to easily deploy a cluster of 3DF workers. It uses the latest version of 3DF.

The Docker Image is freely available at [](https://cloud.docker.com/repository/docker/clockworksio/3df/).

## Build Instructions

Build, tag and publish:

```bash
docker build -t 3df .
docker tag 3df clockworksio/3df:<tag>
docker push clockworksio/3df:<tag>
```

You may need to login to the registry prior to pushing with `docker login`.

## Configuration

Please take a look at the `docker-compose.example.yml` file on how to run a 3DF cluster.

+ **NUM_WORKER**: the number of worker threads per process
+ **NUM_PROCESSES**: the number of processes, that is the number of cluster nodes
+ **ID_PROCESS**: the id of the current node within the cluster. must be unique per cluster
+ **PORT**: port used for node to node communication, defaults is 6262
+ **HOSTS**: semicolon separated list of cluster nodes. a cluster node consists of _<hostname>:<port>_. e.g. "3df-process-01:6262;3df-process-02:6262"

For additional info on how to configure 3DF and Timely Dataflow take a look at the docs for [3DF](https://github.com/comnik/declarative-dataflow#Configuration) and [Timely Dataflow](https://github.com/TimelyDataflow/timely-dataflow#execution).

# License

Dockerfile and examples distributed under the [Apache 2.0 License](https://www.apache.org/licenses/LICENSE-2.0).
