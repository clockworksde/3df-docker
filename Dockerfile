FROM rust:1.34

RUN apt-get update -y \
	&& apt-get upgrade -y \
	&& apt-get install -y ca-certificates ssh git

RUN mkdir /root/.ssh \
	&& touch /root/.ssh/known_hosts \
	&& ssh-keyscan github.com >> /root/.ssh/known_hosts

RUN cd /opt \
	&& git clone https://github.com/comnik/declarative-dataflow.git \
	&& cd declarative-dataflow \
	&& git checkout d0b7fc818a2c446f950a805cd23fc467175935aa

WORKDIR /opt/declarative-dataflow/server

RUN cargo build --release --features "real-time,csv-source"

COPY run.sh /run.sh
RUN chmod +x /run.sh

CMD ["/run.sh"]
