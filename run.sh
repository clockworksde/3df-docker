#!/bin/bash
echo "starting 3df worker..."

array=(${HOSTS//;/ })
truncate -s 0 /hostfile.txt
for i in "${!array[@]}"
do
    echo ${array[i]} >> /hostfile.txt
done

./target/release/declarative-server -w ${NUM_WORKER} -h /hostfile.txt -n ${NUM_PROCESSES} -p ${ID_PROCESS} -- --port=${PORT}
